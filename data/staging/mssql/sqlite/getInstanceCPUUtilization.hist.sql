SELECT
	 t.[EventDateTime]                AS [EventDateTime]
	,t.[ServerName]                   AS [ServerName]
	,t.[RecordTimeStamp]              AS [RecordTimeStamp]
	,t.[RecordRingBufferType]         AS [RecordRingBufferType]
	,t.[MemoryUtilization]            AS [MemoryUtilization]
	,t.[SystemIdle]                   AS [SystemIdle]
	,t.[SQLProcessCPUUtilization]     AS [SQLProcessCPUUtilization]
	,t.[OtherProcessesCPUUtilization] AS [OtherProcessesCPUUtilization]
FROM
	[${getInstanceCPUUtilization}{0}$] t
WHERE
	t.[connectionId] = $connectionId
	AND t.[EventDateTime] > (
		SELECT
			DATETIME(MAX(t.[EventDateTime]), "-1 day")
		FROM
			[${getInstanceCPUUtilization}{0}$] t
		WHERE
			t.[connectionId] = $connectionId
	)
ORDER BY
	 t.[EventDateTime] DESC
	,t.[ServerName] ASC
;
