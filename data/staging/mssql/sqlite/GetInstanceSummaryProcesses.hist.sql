SELECT
	 t.[EventDateTime]               AS [EventDateTime]
	,t.[ServerName]                  AS [ServerName]
	,t.[NumberOfSystemProcesses]     AS [NumberOfSystemProcesses]
	,t.[NumberOfUserProcesses]       AS [NumberOfUserProcesses]
	,t.[NumberOfActiveUserProcesses] AS [NumberOfActiveUserProcesses]
	,t.[NumberOfBlockedProcesses]    AS [NumberOfBlockedProcesses]
FROM
	[${GetInstanceSummaryProcesses}{0}$] t
WHERE
	t.[connectionId] = $connectionId
	AND t.[EventDateTime] > (
		SELECT
			DATETIME(MAX(t.[EventDateTime]), "-1 day")
		FROM
			[${GetInstanceSummaryProcesses}{0}$] t
		WHERE
			t.[connectionId] = $connectionId
	)
ORDER BY
	 t.[EventDateTime] DESC
	,t.[ServerName] ASC
;
