SELECT
	 'VM Reserved' AS `Memory Manager`
	,1000          AS `KB`

UNION ALL

SELECT
	 'VM Committed' AS `Memory Manager`
	,500            AS `KB`
;
