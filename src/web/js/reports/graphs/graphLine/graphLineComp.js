/* global angular */

/**
 * Graph Line Component
 * 
 * @requires BaseGraphComp
 * @requires Collection
 * @requires GraphLineData
 */
app.factory('GraphLineComp', ['BaseGraphComp', 'Collection', 'GraphLineData', function (BaseGraphComp, Collection, GraphLineData) {
	var GraphLineComp = function(){
		BaseGraphComp.call(this);
	};

	angular.extend(GraphLineComp.prototype, BaseGraphComp.prototype, {
		updateReportData: function (data) {
			var graphLine = new Collection(GraphLineData, data);

			return [graphLine.data()];
		}
	});

	return GraphLineComp;
}]);
