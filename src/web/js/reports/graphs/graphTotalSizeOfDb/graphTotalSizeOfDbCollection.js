/* global angular, app */

/**
 * Graph Total Size of DataBase Collection
 *
 * @requires Data
 */
app.factory('GraphTotalSizeOfDbCollection', ['Data', function (Data) {
	var GraphTotalSizeOfDbCollection = function (items) {
		var graphKeys = ['DatabaseSizeMB', 'DatabaseLogSizeMB'];

		this.rows  = [];
		this.ticks = [];

		items.forEach(function (item) {
			for (var i = 0; i < graphKeys.length; i++) {
				if (!this.rows[i]) {
					this.rows[i] = [];
				}

				this.rows[i].push(item[graphKeys[i]]);
			}

			this.ticks.push(item.DatabaseName);
		}.bind(this));
	};

	angular.extend(GraphTotalSizeOfDbCollection.prototype, Data.prototype, {
		data: function () {
			return this.rows;
		}
	});

	return GraphTotalSizeOfDbCollection;
}]);
