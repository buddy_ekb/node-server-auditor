(function (app) {
	app.controller('settingsReportCtrl', ['$scope', '$query', '$routeParams', '$uibModalInstance', function ($scope, $query, $routeParams, $uibModalInstance) {
		$scope.tabs = [];

		$scope.loadInfoRep =  function() {
			$query('paramsrep/' + $routeParams.name ).then(function (res) {
				var data = res.response;
				// console.log('res', data);

				if ( res.length !== 0 ) {
					data.forEach( function (records) {
							// console.log('records', records);
							$scope.tabs.push(records);
						});
					}
				},
				function (err) {
					// console.log('err - ', err);
				});
			}

		$scope.save = function() {
			// console.log('scope.tabs', $scope.tabs );

			$query('paramsrep/save', $scope.tabs, 'PUT' ).then(function (data) {
				// console.log("success", data);
				$uibModalInstance.close('all');
			}, function (res) {
				// console.log('res', res);
			});
		};

		$scope.close = function () {
			$uibModalInstance.dismiss('close');
		};
	}]);
})(app);
