"use strict";

(function (module) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();

	var TQueryResultTable = default_database.define('t_query_result_table',
	{
		// column 'id'
		id: {
			field:         "id",
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       "result table id - primary key"
		},

		// column 'id_recordset'
		id_recordset: {
			field:         "id_recordset",
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    false,
			autoIncrement: false,
			comment:       "record set for the query",
			validate:      {
			}
		},

		// column 'report_table_name'
		report_table_name: {
			field:     "report_table_name",
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   "table name with query results",
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		}
	},
	{
		// define the table's name
		tableName: 't_query_result_table',

		comment: "query results information",

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TQueryResultTable.belongsTo(
				models.t_query, {
					foreignKey: 'id_query'
				}
			);
		},

		indexes: [
			{
				name:   'uidx_t_query_result_table',
				unique: true,
				fields: [
					'id_query',
					'id_recordset'
				]
			}
		]
	}
	);

	module.exports = TQueryResultTable;
})(module);
