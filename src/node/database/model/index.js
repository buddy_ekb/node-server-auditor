"use strict";

(function (module) {
	var tables = {
		connection:           require('./connection'),
		dbSettScheduler:      require('./dbSettScheduler'),
		db_query_parameters:  require('./query_parameters'),
		t_connection_type:    require('./t_connection_type'),
		t_module:             require('./t_module'),
		t_query:              require('./t_query'),
		t_query_result_table: require('./t_query_result_table'),
		db_query_parameters:  require('./query_parameters')
	};

	for(var foreignKeyTable in tables) {
		if (tables[foreignKeyTable].options.hasOwnProperty("associate")) {
			tables[foreignKeyTable].options.associate(tables);
		}
	}

	module.exports = tables;
})(module);
