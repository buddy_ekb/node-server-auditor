"use strict";

(function (module) {
	var Sequelize = require('sequelize');
	var database  = require('src/node/database')();

	var db_query_parameters = database.define('_query_parameters',
		{
			// column 'id'
			id: {
				field:         "id",
				type:          Sequelize.INTEGER,
				allowNull:     false,
				primaryKey:    true,
				autoIncrement: true,
				comment:       "query_parameters id - primary key",
				validate:      {
				}
			},

			// column 'connType'
			connType:{
				field:     "connType",
				type:      Sequelize.STRING,
				allowNull: false,
				comment:   "external server connection type",
				validate: {
					notEmpty: true // don't allow empty strings
				}
			},

			// column 'connName'
			connName:{
				field:     "connName",
				type:      Sequelize.STRING,
				allowNull: false,
				comment:   "external server connection name",
				validate: {
					notEmpty: true // don't allow empty strings
				}
			},

			// column 'repName'
			repName:{
				field:     "repName",
				type:      Sequelize.STRING,
				allowNull: false,
				comment:   "report name",
				validate:  {
					notEmpty: true // don't allow empty strings
				}
			},

			// column 'nameParam'
			nameParam:{
				field:     "nameParam",
				type:      Sequelize.STRING,
				defaultValue: "",
				comment:   "Name parameter",
			},

			// column 'valueParam'
			valueParam:{
				field:        "valueParam",
				type:         Sequelize.STRING,
				allowNull:    true,
				defaultValue: "",
				comment:      "Value parameter",
				validate:     {
								}
			}
		},
		{
			// define the table's name
			tableName: '_query_parameters',

			comment: 'table changed parameter for reports',

			// 'createdAt' to actually be called '_datetime_created'
			createdAt: '_datetime_created',

			// 'updatedAt' to actually be called '_datetime_updated'
			updatedAt: '_datetime_updated',

			// disable the modification of table names; By default, sequelize will automatically
			// transform all passed model names (first parameter of define) into plural.
			freezeTableName: true,

			charset: 'utf8',

			indexes: [
				{
					name:   'uidx_db_query_parameters',
					unique: true,
					fields: [
							'connType',
							'connName',
							'repName',
							'nameParam'
						]
				}
			]
		}
	);

	db_query_parameters.sync({ force: false });

	module.exports = db_query_parameters;
})(module);
