"use strict";

(function (module) {
	var Sequelize = require('sequelize');
	var config    = require('configuration');
	var dbconfs   = config.databases;
	var databases = {};

	for (var dbtitle in dbconfs) {
		var dbconf = dbconfs[dbtitle];

		databases[dbtitle] = new Sequelize(dbconf.basename, dbconf.user, dbconf.password, {
			dialect: 'sqlite',
			logging: false,
			pool: {
				max:  5,
				min:  0,
				idle: 10000
			},
			storage: dbconf.filename
		});
	}

	module.exports = function (dbtitle) {
		return databases[dbtitle == null ? 'default' : dbtitle];
	};
})(module);
