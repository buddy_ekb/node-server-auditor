"use strict";

(function(module) {
	const Q         = require('q');
	const fs        = require('fs');
	const path      = require('path');
	const util      = require('util');
	const dbManager = require('../../../src/node/database');
	const databases = require('../../../configuration').databases;
	var logger      = require('src/node/log');
	const appDir    = path.dirname(require.main.filename);

	function initialize() {
		return runSerialSQLScripts(buildInitScripts());
	}

	function buildInitScripts() {
		const local_data_storages = Object.keys(databases);
		var   sequences           = [];
		var   initFile            = null;
		var   initConfig          = null;
		var   sqlScriptsDir       = null;

		local_data_storages.forEach(function(dbName) {
			initFile = databases[dbName].initialization;

			if (initFile) {
				initConfig = require(initFile);

				sortByOrder(initConfig);

				sqlScriptsDir = path.dirname(initFile);

				sequences = sequences.concat(initConfig.map(function(row) {
					return buildSQLFunction(sqlScriptsDir, row.script, dbName);
				}))
			}
		})

		return sequences;
	}

	function buildSQLFunction(sqlScriptsDir, script, dbName) {
		const sql = getSQLContent(sqlScriptsDir, script);

		const meta = {
			db:     dbName,
			script: script
		};

		return function() {
			logger.info( 'db: [' + dbName + '], script: "' + script + '"');

			return dbManager(dbName)
				.query(sql)
				.catch(onError(meta))
		}
	}

	function getSQLContent(sqlScriptsDir, sqlFile) {
		const strFullSQLInitFile = path.normalize(path.join(appDir, sqlScriptsDir, sqlFile));

		// logger.info( 'appDir: "' + appDir + '"' );
		// logger.info( 'sqlScriptsDir: "' + sqlScriptsDir + '"' );
		// logger.info( 'sqlFile: "' + sqlFile + '"' );
		// logger.info( 'strFullSQLInitFile: "' + strFullSQLInitFile + '"' );

		return fs.readFileSync(strFullSQLInitFile).toString();
	}

	function runSerialSQLScripts(funcs) {
		return funcs.reduce(Q.when, Q([]));
	}

	function onError(data) {
		return function(err) {
			logger.error( err );
			logger.error( 'Error message: "' + err.message + '"' );
			logger.error( 'Error running init script: "' + data.script + '" for database: [' + data.db + ']');

			process.exit(-1);
		}
	}

	function sortByOrder(data) {
		data.sort(function(a, b) {
			return Number(a.order) - Number(b.order);
		})
	}

	module.exports = initialize;
})(module);
