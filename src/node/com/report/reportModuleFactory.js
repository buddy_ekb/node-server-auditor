// "use strict";

(function (module, Object) {
	var Q           = require('q');
	var extend      = require('extend');
	var fs          = require('fs');
	var requireDir  = require('require-dir');
	var path        = require('path');
	var Data        = require('src/node/com/data');
	var initConfig  = require('src/node/database/initial');
	var db          = require('src/node/database')();
	var logger      = require('src/node/log');
	var ParamsUtils = require('src/node/com/paramsrep/paramsUtils');
	var appDir      = path.dirname(require.main.filename);
	var modulesPath = path.join(appDir, 'data');

	var ReportModuleFactory = function (name, serverVersion) {
		// logger.info('reportModuleFactory.js:ReportModuleFactory:name: ', name);

		ReportModuleFactory.getAvailable().then( function( available ) {
			if (available.indexOf(name) === -1) {
				throw 'no report modules defined';
			}
		} );

		this.name = name;

		var json = require(path.join(modulesPath, this.name));

		return new Data.ReportModule({
			name:        json.name,
			type:        json.type,
			description: json.description,
			reports:     mapReports(json.reports, serverVersion),
			repJson:     jsonMenu(json.reports, serverVersion)
		});

		function jsonMenu(reports, serverVersion){
			var report     = null;
			var collection = new Data.ReportCollection();
			var rows       = [];
			var currentRow = null;

			for(var key in reports) {
				currentRow = {
					reports: [],
					section: key
				};

				reports[key].forEach(function (reportPath) {
					var fileName = '';

					report = require(path.join(modulesPath, reportPath));

					fileName = ParamsUtils.getFileNameFromVersion( report.file, serverVersion );

					currentRow.reports.push({
						'name'    : report.name,
						'pathSql' : reportPath,
						'file'    : report.file
					});
				});

				rows.push(currentRow);
			}

			return rows;
		}

		function mapReports(reports, serverVersion) {
			var collection = new Data.ReportCollection();

			for (var key in reports) {
				reports[key].forEach(function (sectionRep) {
					var report    = require(path.join(modulesPath, sectionRep));
					var reportDir = getReportDir(path.join(modulesPath, sectionRep));
					var fileName  = '';

					report.pathSql = sectionRep;

					fileName = ParamsUtils.getFileNameFromVersion( report.file, serverVersion );

					if( fileName == null || fileName == '' ){
						return false;
					}

					collection.add(
						new Data.ReportItem(report,
							fs.readFileSync( path.join( reportDir, fileName ), 'utf8' ),
							report.show_query_file ? fs.readFileSync( path.join( reportDir, report.show_query_file ), 'utf8' ) : null)
					);
				});
			}

			return collection;
		}

		function getReportDir(reportName) {
			var mask = /\/?(?:.(?!\/))+$/;

			return reportName.replace(mask, '/');
		}
	};

	ReportModuleFactory.getAvailable = function () {
		var reports = [];

		return db.models.t_module.findAll( {
			where: {
				is_active: true
			}
		}).then( function( data ) {
			data.forEach( function(val, idx) {
				reports.push( val.module_name );
			})

			return Q.resolve( reports );
		})
	};

	ReportModuleFactory.getAvailableExtended = function () {
		var kvp    = {};
		var result = [];

		return db.models.t_query.findAll( {
			where: {
				is_active: true
			},
			include: [
				{
					model:   db.models.t_module,
					include: [
						{ model: db.models.t_connection_type }
					]
				},
			]
		} ).then( function( data ) {
			data.forEach( function( val, idx ) {
				if ( kvp.hasOwnProperty( val.t_module.module_name ) ) {
					kvp[val.t_module.module_name].reports.push( val.report_name );
				}
				else {
					kvp[ val.t_module.module_name ] = {
						name:    val.t_module.module_name,
						type:    val.t_module.t_connection_type.connection_type_name,
						reports: [ val.report_name ],
					}
				}
			} );

			for ( var p in kvp ) {
				result.push( kvp[p] );
			}

			return Q.resolve( result );
		} );
	};

	module.exports = ReportModuleFactory;
})(module, Object);
