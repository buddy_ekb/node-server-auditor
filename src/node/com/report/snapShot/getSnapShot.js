"use strict";

(function (module) {
	var Q               = require('q');
	var getModelFromDB  = require('./getModelFromDB');
	var promiseWhile    = require('./promiseWhile');
	var modActs         = require('./modelActions');
	var skipKeys        = require('./skipKeys');

	module.exports = function (database, params) {
		params = params || {};

		var tables = [];

		return modActs.getConnection(params.serverName, true).then(function (connection) {
			var connectionId   = connection.get('id');
			var i              = 0;
			var maxTablesCount = 20;
			var topName        = params.module.get('type') + params.module.get('name') + params.name;

			return promiseWhile(function () {
				return i < maxTablesCount;
			}, function () {
				var dfd = Q.defer();

				getModelFromDB(database, topName + i, params.primaryKeys)
					.then(function (Model) {
						return Model.findAll({
							where:      { connectionId: connectionId },
							attributes: { exclude: ['connectionId'] }
						});
					})
					.then(function (table) {
						var result = table.map(extractItem);

						if (result) {
							tables.push(result);
						}
					})
					.catch(function (err) {
						maxTablesCount = i;
					})
					.finally(function () {
						dfd.resolve();
					});

				i++;

				return dfd.promise;
			});
		})
		.then(function () {
			return tables;
		})
		.catch(function () {
			return tables;
		});
	};

	function extractItem(item) {
		var dataValues = item.dataValues;
		var resItem    = {};

		for (var key in dataValues) {
			if (dataValues.hasOwnProperty(key) && skipKeys.indexOf(key) === -1) {
				resItem[key] = dataValues[key];
			}
		}

		return resItem;
	}
})(module);
