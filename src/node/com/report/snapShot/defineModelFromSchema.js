"use strict";

(function (module) {
	var Sequelize = require('sequelize');
	var extend    = require('extend');

	module.exports = function (database, name, schema, primaryKeys) {
		var newSchema = {};

		extend(newSchema, schema);

		newSchema.connectionId = {
			type: Sequelize.INTEGER
		};

		if (primaryKeys) {
			newSchema.id = Sequelize.INTEGER;

			extend(newSchema.connectionId, {
				primaryKey: true
			});

			newSchema['_hash'] = {
				type: Sequelize.CHAR(32)
			};

			primaryKeys.forEach(function (keyName) {
				if (keyName in newSchema) {
					newSchema[keyName] = {
						type:       newSchema[keyName],
						primaryKey: true
					};
				}
			});
		}

		var model = database.define(name, newSchema);

		return model;
	};
})(module);
