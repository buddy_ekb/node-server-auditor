"use strict";

(function (module) {
	var SnapShot = {
		get:       require('./getSnapShot'),
		set:       require('./setSnapShot'),
		queryShow: require('./queryShow')
	};

	module.exports = SnapShot;
})(module);
