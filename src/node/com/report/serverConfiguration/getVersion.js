(function( module ){
	var fs        = require('fs');
	var path      = require('path');
	var extend    = require('extend');
	var Connector = require('src/node/com/report/connector/');
	var appDir    = path.dirname(require.main.filename);
	var appConfig = require( path.join( appDir, 'configuration.json' ) );

	/**
	 * Get version of SQL server
	 * Use report getver
	 *
	 * @param {object} Server configuration
	 * @param {string} Module name
	 *
	 * @callback {function}
	 */
	module.exports = function( server, module, callback ){
		var connector  = null;
		var prefs      = {};
		var verRequest = '';
		var verFile    = '';
		var type       = null;
		var posType    = -1;
		var iType      = -1;

		module = module.split( '.' );

		posType = module.indexOf( 'mysql' );

		if( posType == -1 ){
			posType = module.indexOf( 'mssql' );
		}

		type = module[posType];

		// console.log( appConfig.initials.supported['1'] );

		iType = ( appConfig.initials.supported['1'] == type ) ? '1' : '2';

		extend( true, prefs, server.get( 'prefs' ) );

		//get from default config
		if( iType == -1 || !( 'version' in appConfig.initials ) ){
			return false;
		}

		if( !( iType in appConfig.initials.version ) ){
			return false;
		}

		if( !( 'versionFile' in appConfig.initials.version[iType] ) ){
			return false;
		}

		verFile = appConfig.initials.version[iType].versionFile;

		// console.log('[37]verFile:', verFile);

		verRequest = fs.readFileSync( path.join(appDir, verFile), 'utf8' );

		// console.log('[53]verRequest:', verRequest);

		if( verRequest === undefined || verRequest == null ){
			return false;
		}

		connector = new Connector[type]( prefs );

		var cn = connector.query( verRequest ).then( function( resp ){
			var ret = '';
			var reg = null;

			if( resp !== undefined ){
				if( type == 'mysql' ){
					ret = resp[0].ServerVersion;
				} else if( type == 'mssql' ){
					ret = resp[0][0].ServerVersion;
				}

				// console.log('[69]ret:', ret);

				if( iType != -1 && 'version' in appConfig.initials ){
					if( iType in appConfig.initials.version ){
						if( 'versionRegex' in appConfig.initials.version[iType] ){
							reg = new RegExp( appConfig.initials.version[iType].versionRegex );
							ret = ret.match( reg );

							if( typeof( ret ) == 'object' ) {
								ret = ret[0];
							} else {
								ret = '0';
							}
						}
					}
				}
			}
			callback( {success: true, ver: ret } );
		}
		,function( respErr ){
			callback( { success: false, ver: '' } );
		} );
	};

})(module);
