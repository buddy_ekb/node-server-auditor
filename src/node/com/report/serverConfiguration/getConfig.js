"use strict";

(function (module) {
	var path       = require('path');
	var logger     = require('src/node/log');
	var validator  = require('validator');
	var Data       = require('src/node/com/data');
	var appDir     = path.dirname(require.main.filename);
	var serversDir = path.join(appDir, 'servers');

	/**
	 * Getting configuration from chosen server
	 * @param {string} name of server
	 * @returns {object Object}
	 */
	module.exports = function (name) {
		var config  = null;
		var prefs   = null;
		var version = null;

		try {
			prefs  = require(path.join(serversDir, validator.whitelist(name, 'A-Za-z0-9_.-')));

			config = new Data.ServerConfiguration(name, prefs, version);
		} catch (err) {
			logger.error('getConfig');
			logger.error(err);

			return;
		}

		return config;
	};
})(module);
