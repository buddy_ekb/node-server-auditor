"use strict";

(function(module){
	var ServerConfiguration = {
		getAvailable:     require('./availableServers'),
		getConfiguration: require('./getConfig'),
		getServerVersion: require('./getVersion')
	};

	module.exports = ServerConfiguration;
})(module);
