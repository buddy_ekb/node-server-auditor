"use strict";

(function (module) {
	var Q             = require('q');
	var util          = require('util');
	var sql           = require('mysql');
	var extend        = require('extend');
	var ConnectorType = require('./');
	var logger        = require('src/node/log');

	var MySQL = function () {
		MySQL.super_.apply(this, arguments);
	};

	util.inherits(MySQL, ConnectorType);

	MySQL.prototype.query = function (query) {
		var dfd        = Q.defer();
		var connection = null;

		connection = sql.createConnection(this.config);

		// connection.connect(function(err) {
		// 	if (err) {
		// 		logger.error(err);
		// 		logger.error('[40]:mysql driver error:connect');
		// 		// logger.info(query);
		// 		dfd.reject(err);
		// 	} else {
		// 		connection.query(query, function (err, rows) {
		// 			if (err) {
		// 				logger.error(err);
		// 				logger.error('[35]:mysql driver error:query');
		// 				// logger.info(query);
		// 				dfd.reject(err);
		// 			} else {
		// 				dfd.resolve(rows);
		// 			}
		// 		});
		// 	}
		// });

		connection.query(query, function (err, rows) {
			if (err) {
				logger.error(err);
				logger.error('[35]:mysql driver error:query');
				// logger.info(query);
				dfd.reject(err);
			} else {
				dfd.resolve(rows);
			}
		});

		connection.end();

		return dfd.promise;
	};

	module.exports = MySQL;
})(module);
