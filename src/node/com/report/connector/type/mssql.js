"use strict";

(function (module) {
	var Q             = require('q');
	var util          = require('util');
	var sql           = require('mssql');
	var ConnectorType = require('./');
	var logger        = require('src/node/log');

	var MS_SQL = function () {
		MS_SQL.super_.apply(this, arguments);

		sql.on('error', function (err) {
			logger.error(err);
			logger.error('[14]:mssql driver error:apply');
		});
	};

	util.inherits(MS_SQL, ConnectorType);

	MS_SQL.prototype.query = function (query) {
		var dfd = Q.defer();

		sql.connect(this.config, function (err) {
			var request = new sql.Request();

			if (err) {
				logger.error(err);
				logger.error('[28]:mssql driver error:connect');
				// logger.info(query);

				dfd.reject(err);
				return;
			}

			// enable multiple recordsets in query
			request.multiple = true;

			request.query(query, function (err, recordset) {
				if (err) {
					logger.error(err);
					logger.error('[40]:mssql driver error:query');
					// logger.info(query);

					dfd.reject(err);
				} else {
					dfd.resolve(recordset);
				}

				sql.close();
			});
		}).on('error', function () {
			logger.error('[51]:mssql driver error:socket connection');
			//logger.info(query);

			this.close();
			dfd.reject({error: 'socket connection error'});
		});

		return dfd.promise;
	};

	module.exports = MS_SQL;
})(module);
