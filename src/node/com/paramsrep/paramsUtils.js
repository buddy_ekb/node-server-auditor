(function(){
	/*
	 * Function, that get file name with version
	 * of DB server
	 *
	 * @param {array} file
	 * @param {string} version
	 * @return {string}
	 */
	module.exports.getFileNameFromVersion = function( file, version ){
		var ret                          = '';
		var strMinVersion                = '';
		var strMaxVersion                = '';
		var arrayCurrentVersionComponent = version.split(".");
		var arrayMinVersionComponent     = [];
		var arrayMaxVersionComponent     = [];
		var boolResult                   = false;

		// console.log('param file:', file);
		// console.log('version:', version);

		for ( var intFileIndex = 0; intFileIndex < file.length; intFileIndex++ ) {
			// extract minimum version components
			if ( !( 'min' in file[intFileIndex] )) {
				strMinVersion = '*';
			}
			else {
				strMinVersion = file[intFileIndex].min;
			}

			// extract maximum version components
			if ( !( 'max' in file[intFileIndex] )) {
				strMaxVersion = '*';
			}
			else {
				strMaxVersion = file[intFileIndex].max;
			}

			// console.log('strMinVersion:', strMinVersion);
			// console.log('strMaxVersion:', strMaxVersion);

			for ( var intVersionComponentIndex = 0; intVersionComponentIndex < arrayCurrentVersionComponent.length; intVersionComponentIndex++ ) {
				// console.log('arrayCurrentVersionComponent[intVersionComponentIndex]:', arrayCurrentVersionComponent[intVersionComponentIndex]);

				// check "min" component
				arrayMinVersionComponent = strMinVersion.split(".");

				if (intVersionComponentIndex < arrayMinVersionComponent.length) {
					if (arrayMinVersionComponent[intVersionComponentIndex] != '*') {
						// console.log('arrayMinVersionComponent[intVersionComponentIndex]:', arrayMinVersionComponent[intVersionComponentIndex]);

						if (parseInt(arrayCurrentVersionComponent[intVersionComponentIndex], 10) >= parseInt(arrayMinVersionComponent[intVersionComponentIndex], 10)) {
							boolResult = true;
						}
						else {
							boolResult = false;
						}
					}
					else {
						boolResult = true;
					}
				}
				else {
					boolResult = true;
				}

				if (boolResult === true) {
					// check "max" component
					arrayMaxVersionComponent = strMaxVersion.split(".");

					if (intVersionComponentIndex < arrayMaxVersionComponent.length) {
						if (arrayMaxVersionComponent[intVersionComponentIndex] != '*') {
							// console.log('arrayMaxVersionComponent[intVersionComponentIndex]:', arrayMaxVersionComponent[intVersionComponentIndex]);

							if (parseInt(arrayCurrentVersionComponent[intVersionComponentIndex], 10) <= parseInt(arrayMaxVersionComponent[intVersionComponentIndex], 10)) {
								boolResult = true;
							}
							else {
								boolResult = false;
							}
						}
						else {
							boolResult = true;
						}
					}
					else {
						boolResult = true;
					}
				}

				if (boolResult === false) {
					break;
				}
			}

			if (boolResult === true) {
				ret = file[intFileIndex].file;
				break;
			}
		}

		// console.log('ret file:', ret);
		return ret;
	};
})();
