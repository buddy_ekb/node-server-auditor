"use strict";

(function (module) {
	var Q            = require('q');
	var async        = require('async');
	var Promise      = require('promise');
	var dbsched      = require('src/node/database/model');
	var Report       = require('src/node/com/report');
	var database     = require('src/node/database');
	var nodeSchedule = require('node-schedule');

	/**
	 * Report class
	 *
	 */
	function Scheduler() {
		this._arr_sched = [];
	}

	/**
	 * Enable determined task
	 *
	 * @param {mixed} record It may be record Object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.enable = function(record, sessionId) {
		var active_schedule = null;
		var recent          = true;
		var dfd             = Q.defer();

		enableByRecord = enableByRecord.bind(this);

		if(typeof record === 'object') {
			enableByRecord(record);
		} else {
			this.getRecord(record).then(enableByRecord, dfd.reject);
		}

		return dfd.promise;

		function enableByRecord (record) {
			var repName    = record.repName;
			var connType   = record.connType;
			var connName   = record.connName;
			var moduleType = record.moduleType;
			var schedule   = record.schedule;
			var nameJob    = this.getCurrNameJob(record);

			active_schedule = nodeSchedule.scheduleJob(nameJob, schedule, function(){
				Report(sessionId).getReportByScheduler(repName, connType, connName, moduleType, recent)
					.then(dfd.resolve, dfd.reject);
			});

			this.updateSelector(record, true)
				.then(dfd.resolve, dfd.reject);
		}
	};

	/**
	 * Disable determined task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.disable = function (record) {
		var dfd  = Q.defer();
		var jobs = this.getStartedJobs();

		disableByRecord = disableByRecord.bind(this);

		if(typeof record === 'object') {
			disableByRecord(record);
		} else {
			this.getRecord(record).then(disableByRecord, dfd.reject);
		}

		return dfd.promise;

		function disableByRecord (record) {
			var currJobName = this.getCurrNameJob(record);

			if (jobs.hasOwnProperty(currJobName) ) {
				jobs[currJobName].cancel();

				this.updateSelector(record, false)
					.then(dfd.resolve, dfd.reject);
			} else {
				dfd.reject({'result': 'job not found'});
			}
		};
	};

	Scheduler.prototype.getCurrNameJob = function(record) {
		return record.connType + record.connName + record.moduleType + record.repName + record.schedule;
	};

	Scheduler.prototype.getStartedJobs = function(){
		return nodeSchedule.scheduledJobs;
	}

	Scheduler.prototype.list = function () {
		var tables = [];
		var dfd    = Q.defer();

		dbsched.dbSettScheduler.findAll({})
			.then( function(table) {
				var result = table.map(extractItem);

				if (result) {
					tables.push(result);
				}

				dfd.resolve(tables);
			})
			.finally(function(){
				dfd.resolve(tables);
			});

		return dfd.promise;

		function extractItem(item) {
			var dataValues = item.dataValues;
			var resItem    = {};

			for (var key in dataValues) {
				if (dataValues.hasOwnProperty(key)) {
					resItem[key] = dataValues[key];
				}
			}

			return resItem;
		};
	};

	Scheduler.prototype.insert = function(record){
		var dfd = Q.defer();

		dbsched.dbSettScheduler.sync().then(function () {
			var promises = [];

			promises.push( new Promise( function( resolve, reject ) {
				dbsched.dbSettScheduler.create({
					connType:   record.connType,
					connName:   record.connName,
					moduleType: record.moduleType,
					repName:    record.repName,
					schedule:   record.schedule,
					isEnabled:  record.isEnabled
				})
				.then( function(res) {
					resolve(res);
				})
				.catch(function(err){
					reject(err);
				});
			}));

			Promise.all( promises ).then( function( resp ){
				dfd.resolve(resp);
			}, function( err ){
				dfd.reject(err);
			});
		});

		return dfd.promise;
	};

	Scheduler.prototype.update = function (id, newRecord, sessionId) {
		return this.getRecord(id).then(function (record) {
			this.disable(record)
				.then(function () {
					return record.updateAttributes(newRecord)
						.then(function(result){
							this.enable(result, sessionId);
						}.bind(this));
				}.bind(this), function () {
					record.updateAttributes(newRecord);
				});
		}.bind(this));
	};

	Scheduler.prototype.delete = function (id) {
		var dfd = Q.defer();

		this.getRecord(id).then(function (record) {
			this.disable(record).finally(function () {
				dbsched.dbSettScheduler.sync().then(function () {
					dbsched.dbSettScheduler.destroy({
						where: {
							id: id
						}
					}).then(dfd.resolve, dfd.reject);
				}, dfd.reject);
			});
		}.bind(this), dfd.reject);

		return dfd.promise;
	};

	Scheduler.prototype.updateSelector = function(record, selector){
		var dfd = Q.defer();

		dbsched.dbSettScheduler.sync().then(function () {
			var promises = [];

			promises.push(function (next) {
				dbsched.dbSettScheduler.find( { where:{ id :  record.id } })
					.then(function (record){
						if(record){
							record.updateAttributes({
								isEnabled: selector
							}).catch(function(err){
								dfd.reject(err);
							});
						}
					}).finally(next);;
			});

			async.auto(promises, function (err, results) {
				if (err) {
					dfd.reject(err);
				} else {
					dfd.resolve(results);
				}
			});
		});

		return dfd.promise;
	};

	Scheduler.prototype.getRecord = function (id) {
		var dfd = Q.defer();

		dbsched.dbSettScheduler.findById(id).then(function (record) {
			if (record) {
				dfd.resolve(record);
			} else {
				dfd.reject('record not found');
			}
		}, dfd.reject);

		return dfd.promise;
	};

	/*
	 * a storage for all schedulers
	 */
	var scheduler_instances = { };

	/*
	 * a factory for report instances
	 */
	module.exports = function (sessionId) {
		if (!(sessionId in scheduler_instances)) {
			scheduler_instances[sessionId] = new Scheduler();
		}

		return scheduler_instances[sessionId];
	};
})(module);
