"use strict";

(function (module) {
	var util  = require('util');
	var Model = require('./');

	var ServerConfiguration = function (name, prefs, version) {
		ServerConfiguration.super_.apply(this, arguments);

		this.set('name',    name);
		this.set('prefs',   prefs);
		this.set('version', version || 0);
	};

	util.inherits(ServerConfiguration, Model);

	module.exports = ServerConfiguration;
})(module);
