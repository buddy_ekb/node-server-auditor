"use strict";

(function (module) {
	var express             = require('express');
	var router              = express.Router();
	var Data                = require('src/node/com/data');
	var Report              = require('src/node/com/report');
	var ReportModuleFactory = require('src/node/com/report/reportModuleFactory');
	var ServerConfiguration = require('src/node/com/report/serverConfiguration');
	var handler;

	router.get('/', function (req, res) {
	});

	router.get('/reports', function (req, res) {
		ReportModuleFactory.getAvailableExtended().then( function( data ) {
			res.json(handler.response({
				config:  Report(req.sessionId).isConfigured() ? Report(req.sessionId).config.data() : null,
				modules: data,
				servers: ServerConfiguration.getAvailable()
			}));
		} );
	});

	router.put('/reports', function (req, res) {
		var config;
		var respConf = null;
		var serverConfig = null;

		if (!Object.keys(req.body).length) {
			res.json(handler.response(Report(req.sessionId).configure(null)));
			return;
		}

		serverConfig = ServerConfiguration.getConfiguration(req.body.server);

		ServerConfiguration.getServerVersion( serverConfig, req.body.module, function( rVer ){
			var ver = '0';

			if( rVer.success ){
				ver = rVer.ver;
			}

			config = new Data.DB_Configuration({
				server:  serverConfig,
				module:  req.body.module,
				version: ver
			});

			if (config.isValid()) {
				respConfig = Report(req.sessionId).configure(config);

				res.json(handler.response(respConf));
			} else {
				res.json(handler.error('wrong configuration'));
			}
		} );
	});

	module.exports = function (resHandler) {
		handler = resHandler;
		return router;
	};
})(module);
